{
    "name": "Quality Certificates",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "mrp",
        "product_pricelist_package",
    ],
    "data": [
        # security
        # data
        # reports
        "reports/mrp_production_certificate_digital.xml",
        "reports/mrp_production_certificate_flexo.xml",
        "reports/mrp_production_certificate_domino.xml",
        "reports/purchase_order.xml",
        # views
        "views/mrp_production_certificate_digital.xml",
        "views/mrp_production_certificate_flexo.xml",
        "views/mrp_production_certificate.xml",
        "views/product_template.xml",
    ],
}
