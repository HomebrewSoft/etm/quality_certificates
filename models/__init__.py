# -*- coding: utf-8 -*-
from . import mrp_production_certificate_digital
from . import mrp_production_certificate_flexo
from . import mrp_production_certificate
from . import product_template
